const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const orderController = require("../controllers/orderController");
const auth = require("../auth");

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/checkout", auth.verify, (req,res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		order: req.body
	};

	orderController.createOrder(data).then(resultFromController => res.send(resultFromController));
});

router.get("/user-details/:userId",(req,res) => {
	userController.getUserDetails(req.params).then(resultFromController => res.send(resultFromController));
});

router.get("/orders", auth.verify, (req, res) => {
	let	data = auth.decode(req.headers.authorization);
	orderController.getAllOrders({isAdmin: data.isAdmin}).then(resultFromController => res.send(
		resultFromController));
});

router.get("/myOrders", auth.verify, (req, res) => {
	let data = auth.decode(req.headers.authorization);
	orderController.getMyOrders(data).then(resultFromController => res.send(
		resultFromController));
});

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	let data = auth.decode(req.headers.authorization);
	userController.setUserAsAdmin(data, req.params).then(resultFromController => res.send(
		resultFromController));
});

module.exports = router;