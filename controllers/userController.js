const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {

		if (result == null) {

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {

				return{ access: auth.createAccessToken(result) }

			} else {

				return false
				
			};
		};
	});
};

module.exports.getUserDetails = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		result.password = "";
		return result;
	})
}

module.exports.setUserAsAdmin = (data, reqParams) => {
	if(data.isAdmin){
		let userAsAdmin = {
			isAdmin: true
		};
		return User.findByIdAndUpdate(reqParams.userId, userAsAdmin).then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		});
	}
	let message = Promise.resolve("Only admin can access this.")
	return message.then((value) => {
		return value
	});
}
